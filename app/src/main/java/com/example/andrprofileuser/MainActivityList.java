package com.example.andrprofileuser;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivityList extends AppCompatActivity {
    private musicAdapter musicAdapter;
    private List<music> list;

    public MainActivityList(){
        list = new ArrayList<>();

        list.add(new music(1, "Đừng chờ anh nữa",R.drawable.dcan, "Tăng Phúc","10M"));
        list.add(new music(2, "Dấu mưa",R.drawable.dm, "Trung Quân","1B"));
        list.add(new music(3, "Sau cơn mê",R.drawable.music, "Lệ QUyên","200T"));
        list.add(new music(4, "IF",R.drawable.vct, "Vũ Cát Tường","120M"));
        list.add(new music(5, "Trời giấu trời mang đi",R.drawable.tgtmd, "Amee","15N"));
        list.add(new music(6, "Em là lí do anh say",R.drawable.elldas, "Bray - Viruss","52B"));
        musicAdapter = new musicAdapter(this, list);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        ListView lv = findViewById(R.id.lvEmployees);
        lv.setAdapter(musicAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivityList.this, "Bài hát đã bị xóa.", Toast.LENGTH_SHORT).show();
            }
        });
        TextView back = findViewById(R.id.textbackbtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityList.this, MainActivitysignin.class);
                startActivity(intent);
            }
        });
        ImageButton imageButton = findViewById(R.id.btnprofile);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityList.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
