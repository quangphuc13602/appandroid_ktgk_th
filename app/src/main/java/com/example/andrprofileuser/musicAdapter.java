package com.example.andrprofileuser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class musicAdapter extends BaseAdapter {
    private Context context;
    private List<music> list;
    public musicAdapter(Context context, List<music> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < 0)
            return null;

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_music_item, null);

        TextView tvname = convertView.findViewById(R.id.lvName);
        TextView tvbref = convertView.findViewById(R.id.lvbrief);
        ImageView ivImage = convertView.findViewById(R.id.imageView);
        TextView totalview = convertView.findViewById(R.id.lvviewer);

        music music = list.get(position);
        tvname.setText(music.getNamesong());
        tvbref.setText(music.getSinger());
        ivImage.setImageResource(music.getImageid());
        totalview.setText(music.getTotalview());
        return convertView;
    }
}
