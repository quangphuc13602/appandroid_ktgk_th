package com.example.andrprofileuser;

public class music {
    private int musicid;
    private String namesong;
    private  int imageid;
    private String singer;
    private String totalview;



    public music() {

    }

    public music(int musicid, String namesong, int imageid, String singer, String totalview) {
        this.musicid = musicid;
        this.namesong = namesong;
        this.imageid = imageid;
        this.singer = singer;
        this.totalview = totalview;
    }

    public int getMusicid() {
        return musicid;
    }

    public void setMusicid(int musicid) {
        this.musicid = musicid;
    }

    public String getNamesong() {
        return namesong;
    }

    public void setNamesong(String namesong) {
        this.namesong = namesong;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getTotalview() {
        return totalview;
    }

    public void setTotalview(String totalview) {
        this.totalview = totalview;
    }
}
