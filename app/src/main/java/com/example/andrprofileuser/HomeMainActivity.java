package com.example.andrprofileuser;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class HomeMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);

        AppCompatButton btnlogin = findViewById(R.id.btn_login_home);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeMainActivity.this, MainActivitysignin.class);
                startActivity(intent);
            }
        });
        AppCompatButton btnsignup = findViewById(R.id.btn_signup_home);
        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(HomeMainActivity.this, MainActivitysignup.class);
                startActivity(intent1);
            }
        });

        ImageView fb, ins, gg;
        TextView tv10, tv9, tv8, tv11;

        fb = findViewById(R.id.fab_facebook);
        gg = findViewById(R.id.fab_google);
        ins = findViewById(R.id.fab_íns);
        tv10 = findViewById(R.id.textView10);
        tv8 = findViewById(R.id.textView8);
        tv9 = findViewById(R.id.textView9);
        tv11 = findViewById(R.id.textView11);

        fb.setTranslationY(300);
        gg.setTranslationY(300);
        ins.setTranslationY(300);
        btnlogin.setTranslationY(300);
        btnsignup.setTranslationY(300);
        tv8.setTranslationX(300);
        tv9.setTranslationX(300);
        tv10.setTranslationX(300);
        tv11.setTranslationX(300);


        float v = 0;
        gg.setAlpha(v);
        ins.setAlpha(v);
        fb.setAlpha(v);
        btnlogin.setAlpha(v);
        btnsignup.setAlpha(v);
        tv8.setAlpha(v);
        tv9.setAlpha(v);
        tv10.setAlpha(v);
        tv11.setAlpha(v);

        fb.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(400).start();
        gg.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(600).start();
        ins.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        btnlogin.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        btnsignup.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        tv8.animate().translationX(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        tv9.animate().translationX(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        tv10.animate().translationX(0).alpha(1).setDuration(1000).setStartDelay(800).start();
        tv11.animate().translationX(0).alpha(1).setDuration(1000).setStartDelay(800).start();



    }
}